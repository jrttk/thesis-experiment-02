import Vue from 'vue'
import App from './App.vue'
import '@/assets/styles.scss'

Vue.config.productionTip = false

import drag from 'v-drag'
Vue.use(drag)

new Vue({
  render: h => h(App),
}).$mount('#app')
